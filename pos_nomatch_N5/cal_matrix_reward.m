Trans = zeros(numOfStates, numOfStates);
Rewas = zeros(numOfStates, numOfStates);
Rewah = zeros(numOfStates, numOfStates);
for i = 1:numOfStates
    act = lowerBoundPolicy(i);
    for j = 1:numOfStates
        Trans(i,j) = P{act}(i,j);
        Rewas(i,j) = Rs{act}(i,j);
        Rewah(i,j) = Rh{act}(i,j);
    end
end
Rewas = Rewas.*Trans;
Rewah = Rewah.*Trans;
state=zeros(1,numOfStates);
state(st2stnum(1,0,0,0))=1-alphaPower;
state(st2stnum(0,1,0,0))=alphaPower;
ITER=20;
rewards_cul =0;
rewardh_cul =0;

for i = 1:ITER
    rewards_cul = rewards_cul + sum(state * Rewas);
    rewardh_cul = rewardh_cul + sum(state * Rewah);
    state = state * Trans;
end
disp(rewards_cul/(rewards_cul+rewardh_cul))