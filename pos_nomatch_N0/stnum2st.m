function [ h, a0 ] = stnum2st(num)
global maxForkLen
% input num, output a, h, fork
num = num - 1;

a0=mod(num, maxForkLen+1);
h = floor(num/(maxForkLen+1));
%h=mod(num, maxForkLen+1);
end