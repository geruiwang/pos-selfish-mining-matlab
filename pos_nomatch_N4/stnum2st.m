function [ h, a0, a1, a2, a3, a4 ] = stnum2st(num)
global maxForkLen
% input num, output a, h, fork
num = num - 1;
a4=mod(num, maxForkLen+1);
num = floor(num/(maxForkLen+1));
a3=mod(num, maxForkLen+1);
num = floor(num/(maxForkLen+1));
a2=mod(num, maxForkLen+1);
num = floor(num/(maxForkLen+1));
a1=mod(num, maxForkLen+1);
num = floor(num/(maxForkLen+1));
a0=mod(num, maxForkLen+1);
h = floor(num/(maxForkLen+1));
%h=mod(num, maxForkLen+1);
end