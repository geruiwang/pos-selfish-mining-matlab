rng('shuffle');

global irrelevant relevant active; 
irrelevant = 0; relevant = 1; active = 2;

adopt = 1; override = 2; match = 3; wait = 4;

ITER=3000;
chain = rand()<=alphaPower;
randomstring = rand(1,ITER)<=alphaPower;
h1 = double(chain==0);
a = double(chain==1);
h2=0;
fork = irrelevant;

for i=1:ITER
    state = st2stnum(h1,a,h2,fork);
    act = lowerBoundPolicy(state);
    %if a>h1+h2
    %    act=override;
    %elseif a<h1+h2
    %    act=adopt;
    %else
    %    act=wait;
    %end
    if act == adopt
        chain=[chain,zeros(1,h1),ones(1,a),zeros(1,h2)];
        h1=0;
        a=0;
        h2=0;
    elseif act == override % use h2+1 part of a to override part h2, use a-(h2+1) part to override same length of h1
        chain=[chain,zeros(1,max(0,h1+h2+1-a)),ones(1,a)];
        h1=0;
        a=0;
        h2=0;
    elseif act == match
        % since gamma is 0, so consider match is the same as wait
    elseif act == wait
        % wait, do nothing
    end
    % phase 2, generate new block
    if randomstring(i)==0
        if a==0
            h1=h1+1;
            fork=relevant;
        else
            h2=h2+1;
            fork=relevant;
        end
    else
        if h2==0
            a=a+1;
            fork=irrelevant;
        else
            chain=[chain,zeros(1,h1)];
            h1=h2;
            h2=0;
            a=1;
            fork=irrelevant;
        end
    end
end
sum(chain)/length(chain)
