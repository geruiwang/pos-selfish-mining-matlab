global maxForkLen;
global numOfStates; numOfStates = (maxForkLen+1) ^4 ;
disp(['numOfStates: ' num2str(numOfStates)]);
% a and h can be 0 to maxForkLen, altogether maxForkLen + 1 values
global alphaPower gammaRatio;
% fork: 0 means irrelevant: match is not feasible, either last block is
% selfish OR honest branch is empty
% 1 means relevant: if a>=h now, match is feasible, e.g. last block is honest
% 2 means active (just perfomed a match)
%global irrelevant relevant active; 
%irrelevant = 0; relevant = 1; active = 2;
% actions: 1 adopt, 2 override, 3 match, 4 wait
choices = 3;
adopt = 1; override = 2; wait = 3;
global rou Wrou lowerBoundRou;
global P Rs Rh;

P = cell(1,choices);
% Rs is the reward for selfish miner
Rs = cell(1,choices);
% Rh is the reward for honest miners
Rh = cell(1,choices);
Wrou = cell(1,choices);
for i = 1:choices
    P{i} = sparse(numOfStates, numOfStates);
    Rs{i} = sparse(numOfStates, numOfStates);
    Rh{i} = sparse(numOfStates, numOfStates);
    Wrou{i} = sparse(numOfStates, numOfStates);
end

% define adopt
P{adopt}(:, st2stnum(1, 0, 0,0)) = 1 - alphaPower;
P{adopt}(:, st2stnum(0, 1, 0,0)) = alphaPower;
for i = 1:numOfStates
    if mod(i, 2000)==0
        disp(['processing state: ' num2str(i)]);
    end
    [ h, a0, a1, a2 ] = stnum2st(i);
    Rh{adopt}(i,st2stnum(1, 0, 0,0)) = h;
    Rh{adopt}(i,st2stnum(0, 1, 0,0)) = h;
    % define override
    if a0 > h
        P{override}(i, st2stnum(1, a0-h-1, 0, 0)) = 1-alphaPower;
        P{override}(i, st2stnum(0, a0-h, 0, 0)) = alphaPower;
        Rs{override}(i, st2stnum(1, a0-h-1, 0, 0)) = h+1;
        Rs{override}(i, st2stnum(0, a0-h, 0, 0)) = h+1;
    elseif a1>h-1 && h>=1%ai>h-i
        P{override}(i, st2stnum(1, a1-h-1+1, 0, 0)) = 1-alphaPower;
        P{override}(i, st2stnum(0, a1-h+1, 0, 0)) = alphaPower;
        Rs{override}(i, st2stnum(1, a1-h-1+1, 0, 0)) = h+1-1;%-i, also state +1 is +i
        Rs{override}(i, st2stnum(0, a1-h+1, 0, 0)) = h+1-1;
        Rh{override}(i, st2stnum(1, a1-h-1+1, 0, 0)) = 1;%i
        Rh{override}(i, st2stnum(0, a1-h+1, 0, 0)) = 1;
    elseif a2>h-2 && h>=2
        P{override}(i, st2stnum(1, a2-h-1+2, 0, 0)) = 1-alphaPower;
        P{override}(i, st2stnum(0, a2-h+2, 0, 0)) = alphaPower;
        Rs{override}(i, st2stnum(1, a2-h-1+2, 0, 0)) = h+1-2;%-i, also state +1 is +i
        Rs{override}(i, st2stnum(0, a2-h+2, 0, 0)) = h+1-2;
        Rh{override}(i, st2stnum(1, a2-h-1+2, 0, 0)) = 2;%i
        Rh{override}(i, st2stnum(0, a2-h+2, 0, 0)) = 2;

    else % just for completeness
        P{override}(i, 1) = 1;
        Rh{override}(i, 1) = 10000;
    end
    % define wait
    if h < maxForkLen && a0 < maxForkLen && a1 < maxForkLen && a2 < maxForkLen
        na0=a0+1;
        na1=0;
        na2=0;
        if h>=1
            na1=a1+1;
        end
        if h>=2
            na2=a2+1;
        end
        P{wait}(i, st2stnum(h, na0, na1, na2)) = alphaPower;
        P{wait}(i, st2stnum(h+1, a0, a1, a2)) = 1-alphaPower;
    else
        P{wait}(i, 1) = 1;
        Rh{wait}(i, 1) = 10000;
    end
end

disp(mdp_check(P, Rs))

epsilon = 0.0001;

lowRou = 0;
highRou = 1;
while(highRou - lowRou > epsilon/8)
    rou = (highRou + lowRou) / 2;
    for i = 1:choices
        Wrou{i} = (1-rou).*Rs{i} - rou.*Rh{i};
    end
    [lowerBoundPolicy reward cpuTime] = mdp_relative_value_iteration(P, Wrou, epsilon/8);
    if(reward > 0)
        lowRou = rou;
    else
        highRou = rou;
    end
end
disp('lowerBoundReward: ')
format long
disp(rou)

%analyse_policy(lowerBoundPolicy)
return;

lowerBoundRou = rou;
lowRou = rou;
highRou = min(rou + 0.1, 1);
while(highRou - lowRou > epsilon/8)
    rou = (highRou + lowRou) / 2;
    for i=1:numOfStates
        [a h fork] = stnum2st(i);
        if a == maxForkLen
            mid1 = (1-rou)*alphaPower*(1-alphaPower)/(1-2*alphaPower)^2+0.5*((a-h)/(1-2*alphaPower)+a+h);
            Rs{adopt}(i, st2stnum(1, 0, irrelevant)) = mid1;
            Rs{adopt}(i, st2stnum(0, 1, irrelevant)) = mid1;
            Rh{adopt}(i, st2stnum(1, 0, irrelevant)) = 0;
            Rh{adopt}(i, st2stnum(0, 1, irrelevant)) = 0;
        elseif h == maxForkLen
            mid1=alphaPower*(1-alphaPower)/((1-2*alphaPower)^2);
            mid2=(alphaPower/(1-alphaPower))^(h-a);
            mid3=(1-mid2)*(0-rou)*h+mid2*(1-rou)*(mid1+(h-a)/(1-2*alphaPower));
            Rs{adopt}(i, st2stnum(1, 0, irrelevant)) = mid3;
            Rs{adopt}(i, st2stnum(0, 1, irrelevant)) = mid3;
            Rh{adopt}(i, st2stnum(1, 0, irrelevant)) = 0;
            Rh{adopt}(i, st2stnum(0, 1, irrelevant)) = 0;
        end
    end
    for i = 1:choices
        Wrou{i} = (1-rou).*Rs{i} - rou.*Rh{i};
    end
    rouPrime = max(lowRou-epsilon/4, 0);
    [upperBoundPolicy reward cpuTime] = mdp_relative_value_iteration(P, Wrou, epsilon/8);
    if(reward > 0)
        lowRou = rou;
    else
        highRou = rou;
    end
end
disp('upperBoundReward: ')
disp(rou)
