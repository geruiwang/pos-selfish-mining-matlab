function [M] = analyse_policy( Policy )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

global irrelevant relevant active; 
irrelevant = 0; relevant = 1; active = 2;

acts = 'aow'; %,'o','m','w'}
global maxForkLen
M = cell(maxForkLen+1,maxForkLen+1);
for i = 1:length(Policy)
    [h, a0, a1, a2] = stnum2st(i);
    
    M{a0+1,h+1} = [M{a0+1,h+1},acts(Policy(i))];
    %fprintf('%d,%d: %s\n',h, a0, acts(Policy(i)));
end
%for i = 1:maxForkLen+1
%    for j = 1:maxForkLen+1
%        M{i,j} = [count(M{i,j},'a'),count(M{i,j},'o'),count(M{i,j},'w')]
%    end
%end
end