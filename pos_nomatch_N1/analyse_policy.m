function [M] = analyse_policy( Policy )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

global irrelevant relevant active; 
irrelevant = 0; relevant = 1; active = 2;

acts = 'aow'; %,'o','m','w'}
global maxForkLen
M = cell(maxForkLen+1,maxForkLen+1);
for i = 1:length(Policy)
    [h, a0, a1] = stnum2st(i);
    if a0>=a1
        M{a0+1,h+1} = [M{a0+1,h+1},acts(Policy(i))];
    end
    %fprintf('%d,%d: %s\n',h, a0, acts(Policy(i)));
end
end

