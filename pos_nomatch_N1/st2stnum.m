function stnum = st2stnum(h, a0, a1)
global maxForkLen
% input a, h, fork, output the number of the state
% fork: 0 means irrelevant, 1 means relevant, 2 means active
    if h>maxForkLen || a0>maxForkLen || a1>maxForkLen
        error('the block fork is too long')
    end
    stnum=...
        h*(maxForkLen+1)*(maxForkLen+1)+a0*(maxForkLen+1)+a1 + 1;
end

